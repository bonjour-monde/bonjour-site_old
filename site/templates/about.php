<?php snippet('header') ?>
    <?php snippet('menu') ?>

    <div id="main" role="main">

    <div id="about-intro">
        <div id="about-text">
               <?php echo $page->texttop()->kirbytext()?>
            </div>
        <div class="infoabout">
              <div class="infoabout-left">
              <?php echo $page->textleft()->kirbytext()?>
              </div>
              <div class="infoabout-middle">
              <?php echo $page->textmiddle()->kirbytext()?>
              </div>
              <div class="infoabout-right">
              <?php echo $page->textright()->kirbytext()?>
              </div>
        </div>
    </div>


</div>
</div>
  <?php snippet('footer') ?>
