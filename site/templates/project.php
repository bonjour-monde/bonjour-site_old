<?php snippet('header') ?>
    <?php snippet('menu') ?>


    <div id="main" role="main" >


        <div class="text-container" >
        <div class="text">
        <strong><?php echo $page->title()?></strong>
                 <br>
        <div class="infos">
            <div class="infotxt">
              <?php echo $page->text()->kirbytext()?>
            </div>
            <div class="infolink">
                   <span class="typeinfo">
                       <?php echo $page->type()->kirbytext()?>
                   </span>
                   •
                    <span class="informations">
                  <?php echo $page->year()->kirbytext()?>
                   <?php if($page->place()->isNotEmpty()): ?><?php echo $page->place()?><br><?php endif ?>
                   <?php echo $page->city()->kirbytext()?></span>
                   <?php if($page->links()->isNotEmpty()): ?>
                   <?php echo "•<br>" . $page->links()->kirbytext()?><?php endif ?>
                   <?php if($page->who()->isNotEmpty()): ?>
                   <?php echo "•<br><span class='participants'>" . $page->who()->kirbytext() . "</span>" ?><?php endif ?>
                   <?php if($page->copyright()->isNotEmpty()): ?>
                   <?php echo "<span class='participants'>" . $page->copyright()->kirbytext() . "</span>" ?><?php endif ?>
            </div>
        </div>
        </div>
</div>

   <div class="picture piclist">
      <?php
$images = $page->images();
$first = $images->first();
?>
<?php foreach($images->not('thumb.jpg') as $image): ?>
            <img src="<?php echo $image->url() ?>" alt="">
<?php endforeach ?>
    </div>

  </div>
  <div  class="backcontainer">
      <button onclick="topFunction()" id="backtotop" title="Go to top">↑ RETOUR ↑</button>
  </div>

<?php snippet('footer') ?>
