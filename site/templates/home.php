<?php snippet('header') ?>

  <div class="container">
    <p class="big_txt outline elem">Bonjour Monde</p>
    <p class="small_txt">
      Mène une démarche<br>
      expérimentale, ouverte<br>
      et documentée
    </p>
    <p class="big_txt plain elem">recherche</p>
    <p class="small_txt">
      Questionne les habitudes<br>
      et les outils,<br>
      logiciels et matériels
    </p>
    <p class="big_txt plain elem">des procédés alternatifs</p>
    <p class="small_txt">
    Explore, transmet
    <br>et partage au travers<br>
    d'évènements et d'ateliers
    </p>
    <p class="big_txt plain elem">dans le champ de la création graphique</p>

    <?php foreach($pages->find("projets")->children()->visible() as $page): ?>
      <p class="big_txt outline elem">›</p>
      <p style="display: inline-block;" class="big_txt plain elem title"><?php echo $page->title(); ?> </p>
      <a href="<?php echo $page->url(); ?>" class="work">
        <img class="thumbnail" src="<?php echo $page->images()->first()->url(); ?>" alt="">
      </a>
    <?php endforeach ?>

    <p class="big_txt plain elem contact"> </p>

  </div>
  <a href="about" class="outline elem contact">?</a>
    <p class="plain elem contact"> </p

</div>
  <?php snippet('footer') ?>
