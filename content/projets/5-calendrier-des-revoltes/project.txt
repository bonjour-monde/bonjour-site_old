Title: Calendrier des révoltes

----

Type: Collaboration artistique

----

Year: Mai 2017

----

Place: 

----

City: Saint-Denis

----

Text: 

Bonjour Monde a collaboré avec l'artiste français Matthieu Saladin sur la conception du Calendrier des Révoltes 2017, la troisième activation de ce projet dans lequel “les information habituelles relatives à chaque jour laissent place à l’unique mention d’une révolte ayant eu lieu ce même jour.”

Produit par le centre d'art [Synesthésie](https://www.facebook.com/synesthesie.saintdenis/?fref=mentions), le Calendrier 2017 a été dévoilé à Saint-Denis à l'occasion de la performance “La capture de l'inaudible” de Matthieu Saladin, en mai 2017

----

Links: 

-> [Matthieu Saladin](http://www.matthieusaladin.org/)

-> [Synesthésie](http://www.synesthesie.com)

-> [GitLab](https://gitlab.com/bonjour-monde/revolte)

----

Who: 

----

Copyright: 